resource "aws_identitystore_group" "this" {
  display_name      = var.group_name
  identity_store_id = local.identity_store_id
}

resource "aws_ssoadmin_account_assignment" "this" {
  for_each           = var.aws_account_ids_to_bind_to_group
  instance_arn       = local.identity_arn
  permission_set_arn = var.permission_set_arn_to_bind_to_group

  principal_id   = aws_identitystore_group.this.group_id
  principal_type = "GROUP"

  target_id   = each.value
  target_type = "AWS_ACCOUNT"
}

resource "aws_identitystore_group_membership" "this" {
  for_each = var.group_members

  identity_store_id = local.identity_store_id
  group_id          = aws_identitystore_group.this.group_id
  member_id         = each.key
}
