variable "group_name" {
  type        = string
  description = "The name of the group to create"
}

variable "aws_account_ids_to_bind_to_group" {
  type        = set(string)
  description = "AWS account ids that will be binded to group"
}

variable "group_members" {
  type        = set(string)
  description = "Members to add to the group"
  default     = []
}

variable "permission_set_arn_to_bind_to_group" {
  type = string
  description = "The permission set arn to bind to the group"
}