locals {
  identity_store_id = tolist(data.aws_ssoadmin_instances.identity.identity_store_ids)[0]
  identity_arn = tolist(data.aws_ssoadmin_instances.identity.arns)[0]
}
