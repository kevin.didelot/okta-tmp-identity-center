terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.7"
    }
  }

  backend "s3" {
    region  = "eu-west-1"
    bucket  = "ingenico-identity-terraform-state"
    key     = "environments/identity/20_icp_groups/terraform.tfstate"
    encrypt = true
    dynamodb_table = "ingenico-identity-terraform-state-lock"
  }
}
