locals {
  icp_teams = [
    "b2052414-c091-70de-5f4e-9a64077e2f8b", # kevin.didelot@ingenico.com
    "f2156464-d011-7003-5ff8-fb43246ed3ac", # yannick.pereira-reis@ingenico.com
    "42e5e404-0091-7095-342b-75d97a472689", # Nicolas.CORBET@ingenico.com
    "a295f4f4-c051-709b-6441-55d53b2996ff", # Mohamedtahar.BERAHAL@ingenico.com
    "7265f404-60a1-70de-930c-07924149c4b8", # olivier.arnaud.martinez+isp@gmail.com
    "82e5e404-8031-70f8-5fd5-4979dc60eefe", # Quentin.SALLIO@ingenico.com
    "320564f4-8051-7094-0f6f-a1a9cab97d77", # mohamed.ounis@ingenico.com
    "524504d4-b041-7045-15c0-fa7db49e48fb", # sofiane.akkouche@ingenico.com
    #     "0uex4w2yuN8X3Pxx697",                  # Yassine.HAKIM@ingenico.com
  ]
}

module "aws_identity_center_icp_groups" {
  source                   = "../../../../stacks/aws_identity_center_icp_groups"
  icp_devops_admin_members = toset(local.icp_teams)
  icp_devops_members       = toset(local.icp_teams)
  icp_support_members      = toset(local.icp_teams)
  icp_finops_members       = toset(local.icp_teams)
}
