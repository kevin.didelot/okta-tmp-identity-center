resource "aws_ssoadmin_permission_set" "billing_readonly" {
  instance_arn     = local.identity_arn
  name             = "AWSBillingReadOnlyAccess"
  description      = "Permission set with AWSBillingReadOnlyAccess policy attached"
  session_duration = "PT1H"
}

resource "aws_ssoadmin_managed_policy_attachment" "billing_readonly_attachment" {
  instance_arn       = aws_ssoadmin_permission_set.billing_readonly.instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.billing_readonly.arn
  managed_policy_arn = data.aws_iam_policy.AWSBillingReadOnlyAccess.arn
}
