module "identity_center_icp_devops_admin_group" {
  source = "../../modules/aws_identitystore_group"

  group_name                          = "ICP_DevOps_Admin"
  aws_account_ids_to_bind_to_group    = local.active_aws_organization_accounts_ids
  group_members                       = var.icp_devops_admin_members
  permission_set_arn_to_bind_to_group = data.aws_ssoadmin_permission_set.AdministratorAccess.arn
}

module "identity_center_icp_devops_group" {
  source = "../../modules/aws_identitystore_group"

  group_name                          = "ICP_DevOps"
  aws_account_ids_to_bind_to_group    = local.active_aws_organization_accounts_ids
  group_members                       = var.icp_devops_members
  permission_set_arn_to_bind_to_group = data.aws_ssoadmin_permission_set.AWSReadOnlyAccess.arn
}

# module "identity_center_icp_support_group" {
#   source = "../../modules/aws_identitystore_group"

#   group_name                          = "ICP_Support"
#   aws_account_ids_to_bind_to_group    = local.active_bu_aws_organization_accounts_ids
#   group_members                       = var.icp_support_members
#   permission_set_arn_to_bind_to_group = data.aws_ssoadmin_permission_set.AWSReadOnlyAccess.arn
# }

# module "identity_center_icp_finops_group" {
#   source = "../../modules/aws_identitystore_group"

#   group_name                          = "ICP_FinOps"
#   aws_account_ids_to_bind_to_group    = local.finops_accounts_ids
#   group_members                       = var.icp_finops_members
#   permission_set_arn_to_bind_to_group = aws_ssoadmin_permission_set.billing_readonly.arn
# }
