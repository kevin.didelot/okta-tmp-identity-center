locals {
  identity_store_id = tolist(data.aws_ssoadmin_instances.identity.identity_store_ids)[0]
  identity_arn = tolist(data.aws_ssoadmin_instances.identity.arns)[0]
  active_aws_organization_accounts_ids = toset([
    for account in data.aws_organizations_organizational_unit_descendant_accounts.accounts.accounts : account.id if account.status == "ACTIVE"
  ])
  active_bu_aws_organization_accounts_ids = toset([
    for account in data.aws_organizations_organizational_unit_descendant_accounts.bu_accounts.accounts : account.id if account.status == "ACTIVE"
  ])
  finops_accounts_ids = toset(["058264172835"])
}
