data "aws_ssoadmin_instances" "identity" {}

data "aws_ssoadmin_permission_set" "AWSOrganizationsFullAccess" {
  instance_arn = tolist(data.aws_ssoadmin_instances.identity.arns)[0]
  name         = "AWSOrganizationsFullAccess"
}

data "aws_ssoadmin_permission_set" "AdministratorAccess" {
  instance_arn = tolist(data.aws_ssoadmin_instances.identity.arns)[0]
  name         = "AdministratorAccess"
}

data "aws_ssoadmin_permission_set" "AWSReadOnlyAccess" {
  instance_arn = tolist(data.aws_ssoadmin_instances.identity.arns)[0]
  name         = "AWSReadOnlyAccess"
}

data "aws_organizations_organization" "org" {}

data "aws_organizations_organizational_unit_descendant_accounts" "accounts" {
  parent_id = data.aws_organizations_organization.org.roots[0].id
}

data "aws_organizations_organizational_unit" "BusinessUnit_ou" {
  parent_id = data.aws_organizations_organization.org.roots[0].id
  name      = "BusinessUnit"
}

data "aws_organizations_organizational_unit_descendant_accounts" "bu_accounts" {
  parent_id = data.aws_organizations_organizational_unit.BusinessUnit_ou.id
}

data "aws_iam_policy" "AWSBillingReadOnlyAccess" {
  arn = "arn:aws:iam::aws:policy/job-function/Billing"
}
