variable "icp_devops_admin_members" {
  type        = set(string)
  description = "DevOps admin set of member ids"
}

variable "icp_devops_members" {
  type        = set(string)
  description = "DevOps set of member ids"
}

variable "icp_support_members" {
  type        = set(string)
  description = "Support set of member ids"
}

variable "icp_finops_members" {
  type        = set(string)
  description = "FinOps set of member ids"
}
